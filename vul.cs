using System;
using System.Data.SqlClient;
using System.Web;

namespace VulnerableApp
{
    public class VulnerableCode
    {
        private string connectionString = "your_connection_string_here";

        // SQL Injection vulnerability
        public string GetUserData(string userId)
        {
            string query = "SELECT * FROM Users WHERE UserId = '" + userId + "'"; // Vulnerable to SQL Injection
            string userData = string.Empty;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        userData = reader["UserData"].ToString();
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    // Handle exception
                }
            }

            return userData;
        }

        // XSS vulnerability
        public void DisplayUserData(HttpResponse response, string userData)
        {
            response.Write("<html>");
            response.Write("<body>");
            response.Write("<h1>User Data</h1>");
            response.Write("<p>" + userData + "</p>"); // Vulnerable to XSS
            response.Write("</body>");
            response.Write("</html>");
        }

        public static void Main(string[] args)
        {
            VulnerableCode vc = new VulnerableCode();

            // Simulating user input
            string userInput = "' OR '1'='1"; // SQL Injection payload
            string userData = vc.GetUserData(userInput);

            // Simulating HTTP response
            HttpResponse response = new HttpResponse(Console.Out);
            vc.DisplayUserData(response, userData);
        }
    }
}
